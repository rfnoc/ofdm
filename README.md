# OFDM

Приёмник и передатчик OFDM. За основу взят пример из Gnuradio ofdm_loopback

Оригинальные, но переименованные файлы из Воронежа.

![](img/tx_usrp_0.png) [ofdm_tx_usrp.grc](ofdm_tx_usrp.grc) - Передатчик

![](img/rx_usrp_0.png) [ofdm_rx_usrp.grc](ofdm_rx_usrp.grc) - Приёмник


## Приведение в порядок TX

1. Переименован блок Options.
2. Выровнян в одну линию.
3. Удалены блоки "Channel Model", "QT GUI Range freq_offset", "QT GUI Range noise_voltage"
4. TODO Блок "UHD: USRP Sink" поле Sample rate немного странное!!!

## Приведение в порядок TX

1. Переименован блок Options.
2. Выровнян в одну линию
3. Удалены блоки "QT GUI Range freq_offset", "QT GUI Range noise_voltage"
4. TODO Sample rate тоже немного странный!!!!
